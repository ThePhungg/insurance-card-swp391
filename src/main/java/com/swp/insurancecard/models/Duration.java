package com.swp.insurancecard.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity(name = "durations")
public class Duration {
    @Id
    @Column(name = "duration_id")
    Integer id;
    int duration;
}
