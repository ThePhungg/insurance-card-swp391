package com.swp.insurancecard.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity(name = "prices")
public class Price {
    @Id
    @Column(name = "price_id")
    Integer id;
    int price;
}
