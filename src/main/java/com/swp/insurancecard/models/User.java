package com.swp.insurancecard.models;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
@Entity(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id")
    Long id;
    String gender;
    String phoneNumber;
    String address;
    String firstName;
    String middleName;
    String lastName;
}
