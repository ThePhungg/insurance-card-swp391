package com.swp.insurancecard.repositories;

import com.swp.insurancecard.models.Price;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PriceRepository extends JpaRepository<Price, Integer> {
}
