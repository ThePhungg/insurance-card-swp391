package com.swp.insurancecard.repositories;

import com.swp.insurancecard.models.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public interface AccountRepository extends JpaRepository<Account, Long> {


    @Query("select a from Account a where a.username = :username")
    public Account getAccountByUserName(@Param("username") String username);


}
