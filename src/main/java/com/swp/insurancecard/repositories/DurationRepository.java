package com.swp.insurancecard.repositories;

import com.swp.insurancecard.models.Duration;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DurationRepository extends JpaRepository<Duration, Integer> {
}
