package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Account;
import com.swp.insurancecard.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
//@RequestMapping("/register")
public class RegisterController {
    @Autowired
    AccountRepository accountRepository;
    @GetMapping("/register")
    public String viewRegister(Model model){

        Account account = new Account();
        model.addAttribute("account",account);
        return "register";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveAccount(@ModelAttribute("account") Account account, Model model){
        Account storeAcc = accountRepository.getAccountByUserName(account.getUsername());
        if(storeAcc != null){
            model.addAttribute("msgError","Tên đăng nhập đã tồn tại");
            return "register";
        }
        account.encodePassword();
        accountRepository.save(account);
        return "login";
    }
}
