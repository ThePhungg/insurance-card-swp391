package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Duration;
import com.swp.insurancecard.models.Insurance;
import com.swp.insurancecard.models.Price;
import com.swp.insurancecard.repositories.DurationRepository;
import com.swp.insurancecard.repositories.InsuranceRepository;
import com.swp.insurancecard.repositories.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/productDetail")
public class ProductDetailController {
    @Autowired
    InsuranceRepository insuranceRepository;
    @Autowired
    PriceRepository priceRepository;

    @Autowired
    DurationRepository durationRepository;

    @GetMapping("/{id}")
    public String viewProductDetail(@PathVariable Integer id, Model model){
        Insurance insurance = insuranceRepository.getReferenceById(id);
        List<Price> priceList = priceRepository.findAll();
        List<Duration> durationsList = durationRepository.findAll();
        model.addAttribute("insurance",insurance);
        model.addAttribute("pricelist", priceList);
        model.addAttribute("durationlist", durationsList);
        return "productDetail";
    }
}
