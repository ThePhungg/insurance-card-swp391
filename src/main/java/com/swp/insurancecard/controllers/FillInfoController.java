package com.swp.insurancecard.controllers;

import com.swp.insurancecard.models.Account;
import com.swp.insurancecard.models.User;
import com.swp.insurancecard.repositories.AccountRepository;
import com.swp.insurancecard.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
@RequestMapping("/fill_info")
public class FillInfoController {

    @Autowired
    AccountRepository accountRepository;
    @Autowired
    UserRepository userRepository;
    @GetMapping("")
    public String fillInfoView(Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        if (account.getInfoStatus() == 0){
            User user = new User();
            model.addAttribute("user",user);
            return "fillInfo";
        }
        return "redirect:/home";
    }

    @PostMapping("/save")
    public String saveInfo(@ModelAttribute("user") User user){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Account account = accountRepository.getAccountByUserName(currentPrincipalName);
        User user1 = userRepository.getReferenceById(account.getId());
        user1.setAddress(user.getAddress());
        user1.setEmail(user.getEmail());
        user1.setGender(user.getGender());
        user1.setFirstName(user.getFirstName());
        user1.setMiddleName(user.getMiddleName());
        user1.setLastName(user.getLastName());
        user1.setPhoneNumber(user.getPhoneNumber());
        user1.setDOB(user.getDOB());
        account.setInfoStatus(1);
        accountRepository.save(account);
        userRepository.save(user1);
        return "redirect:/home";

    }

}
