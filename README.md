# Insurance Card website project for SWP391 subject

## Home screen
![image.png](./screens/image.png)

## Detail product screen
![detail.png](./screens/detail.png)

## Login screen
![login.png](./screens/login.png)

## Register screen
![register.png](./screens/register.png)

## Reset password screen
![forgotPass.png](./screens/forgotPass.png)

## Fill user info for account login for the first time screen
![fillInfo.png](./screens/fillInfo.png)

## User dashboard screen
![userDashboard.png](./screens/userDashboard.png)

## Staff dashboard screen
![staffDashboard.png](./screens/staffDashboard.png)
